$(document).ready(function () {

    /*$(".navbar-toggler-icon").on("click", function (e) {
        console.log("we are here");
    })*/

    function menuOpen() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += "responsive";
        } else {
            x.className = "topnav";
        }
    }

    let currID = "";
    $(".member-popup").hide();
    $(".triangle-up").hide();

    // Beim Starten
    if($(window).width() >= 1080) {
        console.log($(window).width());
        console.log("SHOW DESKTOP TEAM");
        $(".desktop-team").show();
        $(".mobile-team").hide();
    } else {
        console.log($(window).width());
        console.log("SHOW MOBILE TEAM");
        $(".mobile-team").show();
        $(".desktop-team").hide();
    }

    // Beim Fenstergröße ändern
    $(window).resize(function () {
        if($(window).width() >= 1080) {
            // console.log($(window).width());
            // console.log("SHOW DESKTOP TEAM");
            $(".desktop-team").show();
            $(".mobile-team").hide();
        } else {
            // console.log($(window).width());
            // console.log("SHOW MOBILE TEAM");
            $(".mobile-team").show();
            $(".desktop-team").hide();
        }
    });

    $(".member").on("click", (e) => {
        let clickedID = e.currentTarget.id;
        console.log(e.currentTarget.id);

        $(".member-popup").hide();
        $(".triangle-up").hide();

        // console.log($("#" + clickedID + " > .triangle-up"));
        // console.log($("#popup-" + clickedID));
        $("#" + clickedID + " > .triangle-up").show();
        $("#popup-" + clickedID).show();
        $("#popup-" + clickedID + "-mobile").show();

        if(clickedID == currID) {
            $(".member-popup").hide();
            $(".triangle-up").hide();
            currID = "";
            return;
        }
        currID = clickedID;
    });

    $(".close-popup").on("click", function() {
        $(".member-popup").hide();
        $(".triangle-up").hide();
        currID = "";
    })

    $("#companypartner_container").hide();
    $("#btn-switch-partner :input").change(function () {
        if(this.id === "toggle_companypartner") {
            $("#companypartner_container").show();
            $("#researchpartner_container").hide();
        } else if(this.id === "toggle_researchpartner") {
            $("#companypartner_container").hide();
            $("#researchpartner_container").show();
        } else {
            console.log("Something went wrong on toggle partner container!");
        }
    });
});